package com.example.lightsblaze;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;



public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private MyListAdapter myListAdapter;
    private DataBaseHelper databaseHandler;
    private boolean huebulb = true, mainLightBulb = false, lifxBulb = true;
    private String myStrFromJson;
    private List<MyListData> myNewModelList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        recyclerView = findViewById(R.id.recyclerview);

        List<MyListData> myListDataList = new ArrayList<>();
        myListDataList.add(new MyListData("Hue Bulb", R.drawable.hue_light, huebulb));
        myListDataList.add(new MyListData("Main Light", R.drawable.main_light, mainLightBulb));
        myListDataList.add(new MyListData("Lifx Light", R.drawable.lifx_light, lifxBulb));

        databaseHandler = new DataBaseHelper(MainActivity.this);

        if (databaseHandler.doesTableExist("MyTable")) {
            if (databaseHandler != null) {
                //databaseHandler.stringDelete();

                myStrFromJson = databaseHandler.getKEY_MapImage("MyTable");
                if (myStrFromJson != null) {
                    Log.e("keyVal", "myJsontoString " + myStrFromJson);

                    myNewModelList = new Gson().fromJson(myStrFromJson, new TypeToken<List<MyListData>>() {
                    }.getType());
                    if (myNewModelList != null && myNewModelList.size() > 0) {
                        myListAdapter = new MyListAdapter(MainActivity.this, myNewModelList);
                    } else {
                        myListAdapter = new MyListAdapter(MainActivity.this, myListDataList);
                    }
                } else {
                    myListAdapter = new MyListAdapter(MainActivity.this, myListDataList);
                }
            }
        } else {
            myListAdapter = new MyListAdapter(MainActivity.this, myListDataList);
        }

        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(myListAdapter);
    }
}
