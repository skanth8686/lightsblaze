package com.example.lightsblaze;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.List;


public class MyListAdapter extends RecyclerView.Adapter<MyListAdapter.ViewHolder> {
    private List<MyListData> myListData;
    private String textOnOff;
    private Context context;

    // RecyclerView recyclerView;
    public MyListAdapter(Context context, List<MyListData> myListData) {
        this.myListData = myListData;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final MyListData myListModel = myListData.get(position);

        holder.cardimageView.setImageResource(myListData.get(position).getImgId());

        holder.textViewName.setText(this.myListData.get(position).getImageName());

        boolean isModeOnOff = myListData.get(position).isMode();

        if (isModeOnOff == true) {
            textOnOff = "On";
        } else {
            textOnOff = "Off";
        }
        holder.textOnOff.setText(textOnOff);

        final MyListData updatedMyListData = new MyListData(myListModel.getImageName(), myListModel.getImgId(), isModeOnOff);

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DisplayDataActivity.class);
                intent.putExtra("myModel", updatedMyListData);
                intent.putExtra("position", position);
                intent.putExtra("myModelList", (Serializable) myListData);
                context.startActivity(intent);
                Toast.makeText(view.getContext(), "click on item: " + myListModel.getImageName(), Toast.LENGTH_LONG).show();
            }
        });

    }


    @Override
    public int getItemCount() {
        return myListData.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView cardimageView;
        public TextView textOnOff, textViewName;
        public RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.cardimageView = (ImageView) itemView.findViewById(R.id.cardimageView);
            this.textOnOff = (TextView) itemView.findViewById(R.id.textOnOff);
            this.textViewName = (TextView) itemView.findViewById(R.id.textViewName);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativeLayout);
        }
    }
}