package com.example.lightsblaze;

import java.io.Serializable;

public class MyListData implements Serializable {
    private String imageName;
    private int imgId;
    private boolean mode;

    public MyListData(String imageName, int imgId, boolean mode) {
        this.imageName = imageName;
        this.mode = mode;
        this.imgId = imgId;
    }

    public MyListData(String imageName, boolean mode) {
        this.imageName = imageName;
        this.mode = mode;
    }

    public boolean isMode() {
        return mode;
    }

    public void setMode(boolean mode) {
        this.mode = mode;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }
}