package com.example.lightsblaze;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.List;


public class DisplayDataActivity extends AppCompatActivity {
    LinearLayout mScreen;
    private TextView textView;
    private ImageView imageView;
    private Switch aSwitch;
    private DataBaseHelper databaseHandler;
    private MyListData updatedListData;
    private String myStrToJson;
    private SeekBar seekBar;
    private int seekR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_data);
        getSupportActionBar().hide();

        databaseHandler = new DataBaseHelper(DisplayDataActivity.this);
        final SQLiteDatabase db = databaseHandler.getWritableDatabase();

        final MyListData updatedModelData = (MyListData) getIntent().getSerializableExtra("myModel");

        final List<MyListData> updatedList = (List<MyListData>) getIntent().getSerializableExtra("myModelList");

        final int strPosition = getIntent().getIntExtra("position", 0);

        textView = findViewById(R.id.lightNames);
        imageView = findViewById(R.id.backImage);
        aSwitch = findViewById(R.id.simpleSwitch);

        textView.setText(updatedModelData.getImageName());
        //set the current state of a Switch
        aSwitch.setChecked(updatedModelData.isMode());

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    updatedListData = new MyListData(updatedModelData.getImageName(), updatedModelData.getImgId(), isChecked);
                } else {
                    updatedListData = new MyListData(updatedModelData.getImageName(), updatedModelData.getImgId(), isChecked);
                }
                updatedList.set(strPosition, updatedListData);

                myStrToJson = new Gson().toJson(updatedList);
                Log.e("myStrToJson", "presentJson==> " + myStrToJson);
                if (databaseHandler.doesTableExist("MyTable")) {
                    databaseHandler.insertMyData("MyTable", myStrToJson);
                } else {
                    databaseHandler.onCreate(db);
                }
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DisplayDataActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });


        seekBar = findViewById(R.id.seekBar);
        mScreen = findViewById(R.id.mScreen);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                Toast.makeText(getApplicationContext(), "seekbar progress: " + progress, Toast.LENGTH_SHORT).show();
                updateBackground();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Toast.makeText(getApplicationContext(), "seekbar touch started!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Toast.makeText(getApplicationContext(), "seekbar touch stopped!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateBackground() {
        seekR = seekBar.getProgress();
        mScreen.setBackgroundColor(
                0xff000000
                        + seekR * 0x10000
                        + seekR * 0x100
                        + seekR
        );
    }
}
