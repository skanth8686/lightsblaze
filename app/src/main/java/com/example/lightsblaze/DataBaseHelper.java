package com.example.lightsblaze;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "MyDb";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "MyTable";
    private static final String KEY_ID = "Key_ID";
    private static final String KEY_TABLE_ID = "imgId";
    private static String KEY_Name = "imageName";
    private static String KEY_Caption = "Caption";

    private static String KEY_MapImage = "mode";

    private static String CREATE_SURVEY_TABLE = "CREATE TABLE " + TABLE_NAME + "(" +
            KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
            KEY_MapImage + " TEXT)";

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_SURVEY_TABLE);
        Log.e("onCreate", "query " + CREATE_SURVEY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

//    public void CreateSurveyTable(SQLiteDatabase db) {
//        db.execSQL(CREATE_SURVEY_TABLE);
//    }

//    public void createModuleTable(SQLiteDatabase db, String tableName, String value) {
//
//        String selectString = "CREATE TABLE " + tableName + " (" + KEY_ID +
//                " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," + value + " TEXT)";
//        try {
//            db.execSQL(selectString);
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }


    public void CreateSurveyTable(SQLiteDatabase db) {
        db.execSQL(CREATE_SURVEY_TABLE);
    }

    // saving survey Table exact data
    public void insertMyData(String tableName, String myData) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_MapImage, myData);
        try {
            DeleteColumn(tableName);
            database.insert(tableName, null, values);
            Log.e("onCreate", "query " + CREATE_SURVEY_TABLE);
            Log.e("onCreate", "inserted " + database.insert(tableName, null, values));
        } catch (Exception e) {
            e.printStackTrace();
        }
        database.close();
    }


    public void DeleteColumn(String tableName) {
        //SQLiteHelper sqLiteHelper = new SQLiteHelper(context);
        SQLiteDatabase db = this.getReadableDatabase();

        String DeleteQuery = "DELETE FROM " + tableName;
        db.execSQL(DeleteQuery);

    }

    public void stringDelete() {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        sqLiteDatabase.delete(TABLE_NAME, null, null);
        Log.e("onCreate", "deleted " + sqLiteDatabase.delete(TABLE_NAME, new String(), null));
        sqLiteDatabase.close();
    }


    public void deleteRow(String value) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME + " WHERE " + KEY_MapImage + "='" + value + "'");
        db.close();
    }

    public boolean doesTableExist(String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select DISTINCT tbl_name from sqlite_master where tbl_name = '" + tableName + "'", null);

        if (cursor != null) {
            if (cursor.getCount() > 0) {
                cursor.close();
                return true;
            }
        }
        return false;
    }

    public void update(String tableName, String myData) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_MapImage, myData);
        String[] args = new String[]{tableName}; //this is for the condition
        database.update(tableName, values, null, args);//replace id for your condition
    }

    public String getKEY_MapImage(String tableName) {
        String myString = null;
        String query = "select * from " + tableName;
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(query, null);
        if (c.moveToFirst()) {
            do {
                myString = c.getString(c.getColumnIndex(KEY_MapImage));
                Log.e("onCreate", "getString " + c.getString(c.getColumnIndex(KEY_MapImage)));
            } while (c.moveToNext());
        }
        c.close();
        db.close();
        return myString;
    }
}
